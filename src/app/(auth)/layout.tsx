import FirstHalf from 'app/components/auth/FirstHalf';
import React from 'react'
import "./layout.css"
export default function AuthLayout({
    children,
}: Readonly<{
    children: React.ReactNode;
}>) {
    return (
        <>
            <div id='d1'>
                <FirstHalf />
                <div id='d1_2'>
                {children}
                </div>
            </div>
        </>
    );
}