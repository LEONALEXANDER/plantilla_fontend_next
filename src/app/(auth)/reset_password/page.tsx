"use client"
import React, { useState } from 'react'
import "./reset_password.css"
import { Button, Form, FormProps, Input, message } from 'antd'
import Link from 'next/link'
import Image from 'next/image';

export default function ResetPassword() {

    function handleChangePassword() {
        console.log("Click!")
    }

   
    const [messageApi, contextHolder] = message.useMessage();


    type FieldType = {
        email?: string;
    };


    const onFinish: FormProps<FieldType>['onFinish'] = (values) => {
        console.log('Success:', values);
        messageApi.open({
            type: 'success',
            content: 'Verificando correo...',
        });
    };

    const onFinishFailed: FormProps<FieldType>['onFinishFailed'] = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };


    return (
        <>

            <Form
                name="basic"
                initialValues={{ remember: true }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
                id="ResetPassword_1"
            >

                <div id='reset_password_cont'>
                    <Image
                        src="/assets/logo.png"
                        alt="products Marketplace"
                        placeholder="blur"
                        blurDataURL={"a"}
                        width={250}
                        height={250}
                        id="reset_password_image"
                    />

                </div>

                <div className='resetPassword_cont'>
                    <div id='ResetPassword_1_1'>Reinicia tu contraseña</div>
                </div>

                <div className='resetPassword_cont'>

                    <div>
                        Ingrese su correo para reiniciar su contraseña
                    </div>
                    <Form.Item<FieldType>
                        name="email"
                        rules={[{ required: true, message: 'Imgresa un correo válido', type: "email" }]}
                    >
                        <Input />
                    </Form.Item>

                </div>


                <div className='resetPassword_cont ResetPassword_button_cont'>

                    {contextHolder}

                    <Button type="primary" style={{ backgroundColor: "rgb(104, 15, 130)" }} className='linkInResetPassword' onClick={handleChangePassword} htmlType="submit">
                        Cambiar
                    </Button>

                    <Link href="/login" className='linkInResetPassword'>
                        <Button type="dashed" className='ResetPassword_buttons' >
                            Volver
                        </Button>
                    </Link>

                </div>


            </Form>


        </>
    )
}
