"use client"
import React, { useState } from 'react'
import { Input, Checkbox, Button, Form } from 'antd';
import type { FormProps } from 'antd';
import Link from 'next/link';
{/*import "./login.css"*/ }
import Image from 'next/image';
export default function Login() {

    type FieldType = {
        username?: string;
        password?: string;
        remember?: string;
    };

    const onFinish: FormProps<FieldType>['onFinish'] = (values) => {
        console.log('Success:', values);
    };

    const onFinishFailed: FormProps<FieldType>['onFinishFailed'] = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <Form
            id='formCont'
            name="basic"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
        >

            <div id='goPharmaTitleForResponsive'>
                <Image
                    src="/assets/logo.png"
                    alt="products Marketplace"
                    placeholder="blur"
                    blurDataURL={"a"}
                    width={250}
                    height={250}
                    id="reset_password_image"
                />
            </div>

            <div className='formDivs formDivs_fc'>Iniciar sesión</div>
            <div className='formDivs'>


                <label>Tu Correo Electrónico</label>
                <Form.Item<FieldType>
                    name="username"

                    rules={[{ required: true, message: 'Por favor, introduzca el correo', type: "email" }]}
                >

                    <Input />

                </Form.Item>

            </div>
            <div className='formDivs'>

                <label>Contraseña</label>
                <Form.Item<FieldType>
                    name="password"
                    rules={[{ required: true, message: 'Por favor, introduzca su contraseña' }]}
                >

                    <Input.Password />

                </Form.Item>

            </div>

            <Form.Item<FieldType>
                name="remember"
                valuePropName="checked"
            >
                <Checkbox>Recordar contraseña</Checkbox>
            </Form.Item>

            <div className='forgotPassword'>
                <Link href="reset_password">¿Olvidaste tu contraseña?</Link>
            </div>

            <Button type="primary" htmlType="submit" style={{ backgroundColor: "#680f82", width: "100%", marginTop: "3vh" }} className='botonIniciarSesion'>
                Iniciar Sesión
            </Button>

        </Form>
    )
}
