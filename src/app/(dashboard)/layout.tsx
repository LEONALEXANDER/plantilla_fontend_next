"use client";
import React, { useState, useEffect } from "react";
import { Layout, theme } from "antd";
import { HeaderComponent } from "app/components/dashboard/layout/header";
import { SidebarComponent } from "app/components/dashboard/layout/sidebar";
import {
  AppstoreOutlined,
  ContainerOutlined,
  DesktopOutlined,
  MailOutlined,
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  PieChartOutlined,
} from "@ant-design/icons";
import { Button, Menu } from "antd";
import "./layout.css"
const { Content } = Layout;
import 'animate.css';


const items = [
  {
    key: "1",
    icon: <PieChartOutlined />,
    label: "Option 1",
  },
  {
    key: "2",
    icon: <DesktopOutlined />,
    label: "Option 2",
  },
  {
    key: "3",
    icon: <ContainerOutlined />,
    label: "Option 3",
  },
  {
    key: "sub1",
    label: "Navigation One",
    icon: <MailOutlined />,
    children: [
      {
        key: "5",
        label: "Option 5",
      },
      {
        key: "6",
        label: "Option 6",
      },
      {
        key: "7",
        label: "Option 7",
      },
      {
        key: "8",
        label: "Option 8",
      },
    ],
  },
  {
    key: "sub2",
    label: "Navigation Two",
    icon: <AppstoreOutlined />,
    children: [
      {
        key: "9",
        label: "Option 9",
      },
      {
        key: "10",
        label: "Option 10",
      },
      {
        key: "sub3",
        label: "Submenu",
        children: [
          {
            key: "11",
            label: "Option 11",
          },
          {
            key: "12",
            label: "Option 12",
          },
        ],
      },
    ],
  },
];


function LayoutDashboard({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();


  const [ImgSidebar, setImgSidebar] = useState("none")

  const [contImgSidebar, setContImgSidebar] = useState("flex")

  const handleCollapsedChange = (isCollapsed: any) => {
    console.log(`Collapsed changed to: ${isCollapsed}`);
    setImgSidebar(isCollapsed)

    if(isCollapsed){
      setContImgSidebar("dissapear_logo_Cont")
    }else{
        setContImgSidebar("sidebar_logocont")
    }


  };

  
  return (
    <Layout id="layout">
      <div
        id="sidebar"
      >
        <div id="sidebar_logocont" className={contImgSidebar}>
          <img id="imgSidebar" src="https://gopharma.com.ve/wp-content/uploads/2024/04/Logo-GoPharma-Rif-Vector.png" className={ImgSidebar ? "animate__animated animate__fadeOut" : "animate__animated animate__fadeIn"}></img>
        </div>

        <Menu 
          id="sidebar_menu"
          defaultSelectedKeys={["1"]}
          defaultOpenKeys={["sub1"]}
          mode="inline"
          theme="dark"
          inlineCollapsed={collapsed}
          items={items}
          className=""
        />
      </div>
      <Layout id="homeComponent">
      <HeaderComponent 
          collapsed={collapsed}
          toggleCollapse={() => setCollapsed(!collapsed)}
          onCollapsedChange={handleCollapsedChange} // Pasamos la función para manejar el cambio de collapsed
        />
        <Content 
          style={{
            margin: "24px 16px",
            padding: 24,
            minHeight: 800,
            background: colorBgContainer,
            borderRadius: borderRadiusLG,
          }}
        >
          {children}
        </Content>
      </Layout>
    </Layout>
  );
}

export default LayoutDashboard;
