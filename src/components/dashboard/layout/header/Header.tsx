import React, {useEffect} from 'react';
import { Button, Flex, Layout, theme } from 'antd';
import { MenuFoldOutlined, MenuUnfoldOutlined, UserOutlined } from '@ant-design/icons';
import 'animate.css';
import "./Header.css"
import AccountDrawer from "../../../../app/(dashboard)/accountManagment/AccountMagment"
const { Header } = Layout;

const HeaderComponent = ({ collapsed, toggleCollapse, onCollapsedChange }) => {
  const {
    token: { colorBgContainer },
  } = theme.useToken();

  const handleButtonClick = () => {
    toggleCollapse();
    console.log("Button clicked"); 


  };

  // Llama a la función onCollapsedChange cada vez que collapsed cambie
  useEffect(() => {
    onCollapsedChange(collapsed);
  }, [collapsed, onCollapsedChange]);



  return (
    <Header style={{ padding: 0, background: colorBgContainer,}}>

      <div className='Header_1'>
      <Button
        type="text"
        icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
        onClick={handleButtonClick} // Usa la nueva función handleButtonClick
        style={{
          fontSize: '16px',
          width: 64,
          height: 64,
        }}
      />
      
      <img src='https://gopharma.com.ve/wp-content/uploads/2024/04/Logo-GoPharma-Rif-Vector.png' style={{width:"70px"}} className={collapsed? "animate__animated animate__fadeIn":"animate__animated animate__fadeOut"}></img>
      </div>
      <div className='Header_2'>


      <AccountDrawer/>
     

      </div>


    </Header>
  );
};

export default HeaderComponent;
